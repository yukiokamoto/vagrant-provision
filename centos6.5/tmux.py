# -*- coding: utf-8 -*-

from fabric.api import run, sudo, cd, shell_env
from fabric.contrib.files import sed, append, contains

def yum_update():
	sudo("yum update -y")

def install_tmux():
	# install dependent tools
	sudo("yum -y groupinstall 'Development Tools'")
	sudo("yum -y install wget ncurses-devel")

	run("mkdir -p ~/tools")

	# download sources
	with cd("~/tools"):
		run("wget -nc https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz")
		run("wget -nc http://downloads.sourceforge.net/tmux/tmux-1.9a.tar.gz")
		run("tar xvf libevent-2.0.21-stable.tar.gz")
		run("tar xvf tmux-1.9a.tar.gz")

	# build libevent
	with cd("~/tools/libevent-2.0.21-stable"):
		run("./configure")
	run("make -C tools/libevent-2.0.21-stable")
	sudo("make -C tools/libevent-2.0.21-stable install")

	# build tmux
	with cd("~/tools/tmux-1.9a"):
		run("./configure")
	run("LIBEVENT_CFLAGS='-I/usr/local/include' LIBEVENT_LIBS='-L/usr/local/lib -Wl,-rpath=/usr/local/lib -levent' make -C tools/tmux-1.9a")
	sudo("LIBEVENT_CFLAGS='-I/usr/local/include' LIBEVENT_LIBS='-L/usr/local/lib -Wl,-rpath=/usr/local/lib -levent' make -C tools/tmux-1.9a install")
	sudo("ln -s -f /usr/local/lib/libevent-2.0.so.5 /usr/lib64/libevent-2.0.so.5")

	run("echo 'set-option -g prefix C-j' > ~/.tmux.conf")
